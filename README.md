# Cargo Version Control System extension

Cargo-vcs is a Cargo extension that adds version control helpers to help manage workspace members.

## Example use cases

### View current branch and MSRV status for workspace members

- `cargo vcs status` - for brief overview
- `cargo vcs status -v` - for verbose view

```
$ cargo vcs status
Workspace is set to profile: new [.../rust/embedded] MSRV: 1.59.0

** Projects with uncommitted changes **

redv: refs/heads/e-h-1.0.0-alpha.7 [.../rust/embedded/apps/redv]
└── [CHANGES]
    └── src/main.rs

e310x-hal: refs/heads/e-h-1.0.0-alpha.7 [.../rust/embedded/e310/e310x-hal]
└── [CHANGES]
    ├── src/spi/bus.rs
    └── src/spi/config.rs
```

### Switch to a branch or commit on workspace members

- `cargo vcs checkout master` - switch to master on all workspace members
- `cargo vcs checkout master -m member1, member2` - switch on specific members only
```
$ cargo vcs checkout master
redv set to refs/heads/master
embedded-hal set to refs/heads/master
e310x set to refs/heads/master
e310x-hal set to refs/heads/master
hifive1 set to refs/heads/master
riscv set to refs/heads/master
riscv-rt set to refs/heads/master
mipidsi set to refs/heads/master
```

### Working with profiles

- `cargo vcs profile save profile1` - saves current repo state of each workspace member under the name `profile1`
- `cargo vcs profile set profile1` - switches all workspace member repositories to state saved in `profile1`

```
$ cargo vcs profile set new
redv set to refs/heads/e-h-1.0.0-alpha.7
embedded-hal set to refs/tags/v1.0.0-alpha.7
e310x set to refs/heads/e-h-1.0.0-alpha.7
e310x-hal set to refs/heads/e-h-1.0.0-alpha.7
hifive1 set to refs/heads/v0.11.0-alpha.1
riscv set to refs/heads/e-h-1.0.0-alpha.7
riscv-rt set to refs/heads/e-h-1.0.0-alpha.7
mipidsi set to refs/heads/v0.3.0-alpha.1
```

### Navigating to a specific workspace member

- `$ cd $(cargo vcs dir member1)`

## Profile configs

Profile config is saved in the workspace root directory under the name `Cargo_vcs.toml`. Manual editing is possible but using `cargo vcs profile save <profile_name>` is the preferred method as it resolves shortnames to full repository reference paths.

### Profile format

Toml file with the format of

```toml
[vcs.<profile_name>]
<project_name> = "<git_reference>"
...
```

## VCS support

### Git

Git is currently the only supported VCS.

#### Uncommitted changes

Profile and branch switching with uncommitted changes result in new stash pushes. Stashed changes are popped back when returning to the source reference or profile.

Each stash change is saved under a specific name and looked up as such, FIFO is not used and stash changes created outside of cargo-vcs are not considered.

### Others

Currently only git is supported. Other version control systems can be added by implementing the `Repository` trait, however some more work is required in regards to the dirty changes strategy.

## Minimum Supported Rust Version (MSRV)

This crate is guaranteed to compile on stable Rust 1.59.0 and up. It *might*
compile with older versions but that may change in any new patch release.

## Early release

This project is still in early release phases. It should be safe to use but there's always a possibility of bugs which could result in workspace repositories ending up in an odd state.