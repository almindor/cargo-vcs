use std::ffi::OsString;

use clap::Parser;

mod vcs;

use vcs::cli::*;

fn main() {
    // running with cargo vcs causes the vcs arg to be sent along
    // we need to unhack this for Clap
    let args = parse_args();

    let work_dir = args
        .workspace
        .unwrap_or_else(|| std::env::current_dir().expect("Unable to obtain current_dir"));
    let mut vcs = match vcs::Vcs::new(work_dir) {
        Ok(val) => val,
        Err(err) => {
            eprintln!("{}", err);
            std::process::exit(err.exit_code());
        }
    };

    match args.command {
        Commands::Dir { member } => {
            vcs.dir(&member).expect("Error changing directory");
        }
        Commands::Checkout {
            reference,
            projects,
        } => {
            vcs.checkout_ref(&reference, projects)
                .expect("Error setting branch on projects");
        }
        Commands::Profile(ProfileCommand::Save { profile }) => {
            vcs.save_profile(&profile)
                .expect("Error saving vcs profile");
        }
        Commands::Profile(ProfileCommand::Remove { profile }) => {
            vcs.remove_profile(&profile)
                .expect("Error removing vcs profile");
        }
        Commands::Profile(ProfileCommand::Set { profile }) => {
            vcs.set_profile(&profile)
                .expect("Error setting vcs profile");
        }
        Commands::Profile(ProfileCommand::List) => {
            vcs.list_profiles().expect("Error setting vcs profile");
        }
        Commands::Status { verbose } => {
            if verbose {
                vcs.list();
            } else {
                vcs.status().expect("Error summarizing info");
            }
        }
    }
}

fn parse_args() -> Cli {
    // running with cargo vcs causes the vcs arg to be sent along
    // we need to unhack this for Clap
    let mut all_args: Vec<OsString> = std::env::args_os().collect();
    let osvcsstr = OsString::from("vcs");
    if all_args.get(1) == Some(&osvcsstr) {
        all_args.remove(1);
    }
    Cli::parse_from(all_args)
}
