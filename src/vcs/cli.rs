use clap::Parser;
use std::path::PathBuf;

/// A fictional versioning CLI
#[derive(Debug, Parser)]
#[clap(name = "cargo-vcs")]
#[clap(about = "Version control helper for Cargo", long_about = None)]
pub struct Cli {
    #[clap(subcommand)]
    pub command: Commands,
    /// Path to workspace folder, defaults to CWD
    #[clap(short = 'w')]
    pub workspace: Option<PathBuf>,
}

#[derive(Debug, Parser)]
pub enum Commands {
    /// Output absolute directory path of given workspace member root
    Dir {
        /// Workspace member name to show directory for
        member: String,
    },
    /// Show workspace status
    Status {
        /// Show verbose status information
        #[clap(short = 'v')]
        verbose: bool,
    },
    /// Set or save profiles
    #[clap(subcommand)]
    Profile(ProfileCommand),
    /// Checkout specific repo reference (branch, commit etc.)
    Checkout {
        /// Reference name
        reference: String,
        /// Memeber list to apply checkout to, defaults to all
        #[clap(short = 'm')]
        projects: Option<Vec<String>>,
    },
}

#[derive(Debug, Parser)]
pub enum ProfileCommand {
    /// Save the current workspace VCS state to Cargo_vcs.toml under given profile name
    #[clap(arg_required_else_help = true)]
    Save { profile: String },
    /// Remove the given profile configuration
    #[clap(arg_required_else_help = true)]
    Remove { profile: String },
    /// Switch workspace to given profile
    #[clap(arg_required_else_help = true)]
    Set { profile: String },
    /// Lists all saved profiles
    List,
}
