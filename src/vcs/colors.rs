use crossterm::style::Color;

pub const PROJECT_COLOR: Color = Color::Red;
pub const PROFILE_COLOR: Color = Color::Blue;
pub const ERROR_COLOR: Color = Color::DarkRed;
pub const CHANGES_COLOR: Color = Color::Cyan;
pub const VERSION_COLOR: Color = Color::DarkGreen;
pub const REFS_COLOR: Color = Color::DarkYellow;
pub const MSRV_COLOR: Color = Color::DarkCyan;
