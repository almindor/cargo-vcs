use crossterm::style::{StyledContent, Stylize};
use std::fmt::Display;

#[derive(Debug)]
pub enum Error {
    /// Any upstream library error
    Upstream(Box<dyn std::error::Error>),
    /// Cargo.toml specific logical errors
    Cargo(String),
    /// Project/repo status logical errors
    Project(String),
    /// Input selection errors (e.g. bad profile name)
    Input(String),
}

impl From<git2::Error> for Error {
    fn from(ge: git2::Error) -> Self {
        Error::Upstream(Box::new(ge))
    }
}

impl From<std::io::Error> for Error {
    fn from(ie: std::io::Error) -> Self {
        Error::Upstream(Box::new(ie))
    }
}

impl From<toml::de::Error> for Error {
    fn from(tde: toml::de::Error) -> Self {
        Error::Upstream(Box::new(tde))
    }
}

impl From<toml::ser::Error> for Error {
    fn from(tse: toml::ser::Error) -> Self {
        Error::Upstream(Box::new(tse))
    }
}

impl From<semver::Error> for Error {
    fn from(se: semver::Error) -> Self {
        Error::Upstream(Box::new(se))
    }
}

impl From<Box<dyn std::error::Error>> for Error {
    fn from(other: Box<dyn std::error::Error>) -> Self {
        Error::Upstream(other)
    }
}

impl Error {
    pub fn cargo_error(line: &str) -> Self {
        Error::Cargo(line.into())
    }

    pub fn input_error(line: &str, value: &str) -> Self {
        Error::Input(format!("{}: {}", line, value))
    }

    pub fn project_error(line: &str) -> Self {
        Error::Project(line.into())
    }

    pub fn exit_code(&self) -> i32 {
        match self {
            Self::Upstream(_) => 10,
            Self::Cargo(_) => 11,
            Self::Project(_) => 12,
            Self::Input(_) => 13,
        }
    }

    fn to_styled(&self) -> StyledContent<String> {
        match self {
            Self::Upstream(e) => e.to_string().red(),
            Self::Cargo(e) => e.to_string().cyan(),
            Self::Project(e) => e.to_string().yellow(),
            Self::Input(e) => e.to_string().dark_red(),
        }
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.to_styled())
    }
}
