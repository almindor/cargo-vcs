use std::path::Path;

use super::{Error, Repository};

pub struct GitRepository {
    repo: git2::Repository,
    machine_id: String,
}

impl Repository for GitRepository {
    fn current_ref(&self) -> Result<String, Error> {
        if self.repo.head_detached()? {
            self.match_tag()
        } else {
            let head_ref = self.repo.head()?;
            let name = head_ref
                .name()
                .ok_or_else(|| Error::project_error("Unable to retreive HEAD reference"))?
                .into();

            Ok(name)
        }
    }

    fn checkout(&self, name: &str) -> Result<String, Error> {
        let target_ref = self.repo.resolve_reference_from_short_name(name)?;
        let ref_name = target_ref
            .name()
            .ok_or_else(|| Error::project_error("Invalid UTF-8 in git reference"))?;

        self.checkout_ref(ref_name)?;
        self.repo.set_head(ref_name)?;

        Ok(ref_name.into())
    }

    fn stash_changes(&mut self, name: &str) -> Result<bool, Error> {
        if self.uncommitted_changes()?.is_empty() {
            return Ok(false);
        }

        let mut err = None;
        let stash_name = self.stash_name(name);
        self.repo.stash_foreach(|_, name, _| {
            if name.contains(&stash_name) {
                err = Some(Error::project_error("Existing stashed changes found"));
                false
            } else {
                true
            }
        })?;

        if let Some(err) = err {
            return Err(err);
        }

        let repo_signature = self.repo.signature()?;
        self.repo.stash_save(&repo_signature, &stash_name, None)?;

        Ok(true)
    }

    fn unstash_changes(&mut self, name: &str) -> Result<bool, Error> {
        let mut found_stash = None;
        let stash_name = self.stash_name(name);

        self.repo.stash_foreach(|idx, name, _| {
            if name.contains(&stash_name) {
                if found_stash.is_none() {
                    found_stash.replace(Ok(idx));
                } else {
                    found_stash
                        .replace(Err(Error::project_error("Multiple stashed changes found")));
                }
            }
            true
        })?;

        match found_stash {
            Some(Ok(idx)) => {
                self.repo.stash_apply(idx, None)?;
                self.repo.stash_drop(idx)?;
                Ok(true)
            }
            Some(Err(err)) => Err(err),
            None => Ok(false),
        }
    }

    fn uncommitted_changes(&self) -> Result<Vec<String>, Error> {
        if self.repo.state() != git2::RepositoryState::Clean {
            let err = format!(
                "{} repository not in clean state",
                self.repo.path().display()
            );
            return Err(Error::Project(err));
        }

        let mut status_options = git2::StatusOptions::new();
        status_options.include_ignored(false);
        status_options.include_untracked(false);

        let iter = self
            .repo
            .statuses(Some(&mut status_options))?
            .iter()
            .map(|s| s.path().unwrap().to_string()) // TODO: bubble this up
            .collect();

        Ok(iter)
    }
}

impl GitRepository {
    pub fn open(path: &Path) -> Result<Box<Self>, Error> {
        let repo = git2::Repository::open(path)?;

        Ok(Box::new(Self {
            repo,
            machine_id: machine_uid::get()?,
        }))
    }

    fn checkout_ref(&self, ref_name: &str) -> Result<(), Error> {
        let ref_val = self.repo.find_reference(ref_name)?;
        let commit = self.repo.reference_to_annotated_commit(&ref_val)?;
        let tree = self.repo.find_object(commit.id(), None)?;
        self.repo.checkout_tree(&tree, None)?;

        Ok(())
    }

    fn stash_name(&mut self, name: &str) -> String {
        format!("cargo-vcs_{}_{}", self.machine_id, name)
    }

    fn match_tag(&self) -> Result<String, Error> {
        let head_ref = self.repo.head()?;

        let commit = head_ref.peel_to_commit()?;
        let mut tag_data = Vec::new();
        self.repo.tag_foreach(|oid, name_u8| {
            tag_data.push((oid, Vec::from(name_u8)));
            true
        })?;

        for raw in tag_data {
            let oid = raw.0;
            let name = std::str::from_utf8(&raw.1)
                .map_err(|_| Error::project_error("Invalid UTF-8 in tag name"))?;

            if let Ok(tag) = self.repo.find_tag(oid) {
                let peeled = tag.peel()?;
                if peeled.id() == commit.id() {
                    return Ok(name.into());
                }
            }
        }

        Err(Error::project_error("Tag not found"))
    }
}
