use std::{
    collections::HashMap,
    fmt::Display,
    path::{Path, PathBuf},
};

use crossterm::style::Stylize;
use enumset::{EnumSet, EnumSetType};
use semver::Version;
use termtree::Tree;

use super::colors::*;
use super::error::Error;
use super::systems::{git::GitRepository, Repository};

pub struct Project {
    path: PathBuf,
    name: String,
    msrv: Option<Version>,
    crate_version: Version,
    pub repo: Box<dyn Repository>,
    profile_map: HashMap<String, String>, // vcs profile name to branch names
}

#[derive(EnumSetType, Debug)]
pub enum ProjectDisplayOptions {
    Profiles,
    Msrv,
    Changes,
}

impl ProjectDisplayOptions {
    pub fn all() -> EnumSet<Self> {
        Self::Profiles | Self::Msrv | Self::Changes
    }
}

pub type Projects<'origin> = std::slice::Iter<'origin, Project>;

impl Project {
    pub fn new(path: PathBuf, profile_map: HashMap<String, String>) -> Result<Self, Error> {
        let name = path
            .components()
            .last()
            .ok_or_else(|| Error::project_error("Unable to get project name"))?
            .as_os_str()
            .to_str()
            .ok_or_else(|| Error::project_error("Project path contains invalid UTF-8"))?
            .to_owned();

        let repo = GitRepository::open(&path)?;
        let msrv = Self::parse_version(&path, "rust-version")?;
        let crate_version = match Self::parse_version(&path, "version")? {
            Some(v) => v,
            None => return Err(Error::cargo_error("Crate version not found")),
        };

        Ok(Self {
            path,
            name,
            msrv,
            crate_version,
            repo,
            profile_map,
        })
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn path(&self) -> &Path {
        &self.path
    }

    pub fn current_ref(&self) -> Result<Option<String>, Error> {
        match self.repo.current_ref() {
            Ok(val) => Ok(Some(val)),
            Err(err) => {
                eprintln!("{}: {}\n", self.name(), err);
                Ok(None)
            }
        }
    }

    pub fn current_profile(&self) -> Result<Option<String>, Error> {
        // repo.current_ref can fail if repo has no head yet (e.g. post init)
        let current_ref = self.current_ref()?;

        if let Some(val) = self
            .profile_map
            .iter()
            .find(|v| Some(v.1) == current_ref.as_ref())
        {
            return Ok(Some(val.0.clone()));
        }

        Ok(None)
    }

    pub fn switch_to_profile(&self, profile_name: &str) -> Result<String, Error> {
        if let Some(ref_name) = self.profile_map.get(profile_name) {
            self.repo.checkout(ref_name)?;
            Ok(ref_name.into())
        } else {
            Err(Error::input_error(
                "Unable to find profile for branch",
                self.name(),
            ))
        }
    }

    pub fn msrv(&self) -> Option<&Version> {
        self.msrv.as_ref()
    }

    fn parse_version(path: &Path, key: &str) -> Result<Option<Version>, Error> {
        let toml_path = path.join("Cargo.toml");
        let toml_contents = std::fs::read_to_string(toml_path)?;

        let cargo: toml::Value = toml::from_str(&toml_contents)?;

        if let Some(pkg) = cargo.get("package") {
            if let Some(value) = pkg.get(key) {
                if value.is_str() {
                    let mut ver_str = value
                        .as_str()
                        .ok_or_else(|| Error::cargo_error("Unparsable version in Cargo.toml"))?
                        .to_owned();
                    let dots = ver_str.chars().filter(|c| *c == '.').count();
                    if !(1..=2).contains(&dots) {
                        return Err(Error::cargo_error("Invalid rust-version in Cargo.toml"));
                    }
                    // TODO: figure out if we can un-hack this
                    if dots == 1 {
                        ver_str += ".0"; // make 1.59 -> 1.59.0 for semver comparisons
                    }
                    let ver_req = Version::parse(&ver_str)?;

                    return Ok(Some(ver_req));
                }
            }
        }

        Ok(None)
    }

    pub fn display(&self, options: EnumSet<ProjectDisplayOptions>) -> Result<Tree<String>, Error> {
        let title = format!(
            "{}@{}: {} [{}]",
            self.name().bold().with(PROJECT_COLOR),
            format!("v{}", self.crate_version).with(VERSION_COLOR),
            self.current_ref()?
                .unwrap_or_else(|| "???".into())
                .with(REFS_COLOR),
            format!("{}", self.path().display()).italic(),
        );
        let mut root = Tree::new(title);

        if options & ProjectDisplayOptions::Profiles == ProjectDisplayOptions::Profiles {
            let mut profiles_tree = Tree::new("[PROFILES]".with(PROFILE_COLOR).to_string());
            let mut count = 0;
            for (profile_name, branch_name) in &self.profile_map {
                let leaf = Tree::new(format!(
                    "{} -> {}",
                    profile_name.as_str().with(PROFILE_COLOR),
                    branch_name.as_str().with(REFS_COLOR)
                ));
                profiles_tree.push(leaf);
                count += 1;
            }

            if count > 0 {
                root.push(profiles_tree);
            }
        }

        if options & ProjectDisplayOptions::Msrv == ProjectDisplayOptions::Msrv {
            if let Some(msrv) = &self.msrv {
                let leaf = Tree::new(msrv.to_string().with(MSRV_COLOR).to_string());
                let mut msrv_tree = Tree::new("[MSRV]".with(MSRV_COLOR).to_string());
                msrv_tree.push(leaf);

                root.push(msrv_tree);
            }
        }

        if options & ProjectDisplayOptions::Changes == ProjectDisplayOptions::Changes {
            let statuses = self.repo.uncommitted_changes()?;
            let uncommitted = statuses
                .iter()
                .map(|path| Tree::<String>::new(path.as_str().with(CHANGES_COLOR).to_string()));
            let mut changes_tree = Tree::new("[CHANGES]".with(CHANGES_COLOR).to_string());
            let mut change_count = 0;
            for change in uncommitted {
                change_count += 1;
                changes_tree.push(change);
            }

            if change_count > 0 {
                root.push(changes_tree);
            }
        }

        Ok(root)
    }
}

impl Display for Project {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            self.display(ProjectDisplayOptions::all())
                .map_err(|_| std::fmt::Error)?
        )
    }
}
