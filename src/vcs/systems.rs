pub mod git;

use super::Error;

pub trait Repository {
    fn current_ref(&self) -> Result<String, Error>;

    fn checkout(&self, ref_name: &str) -> Result<String, Error>;

    fn stash_changes(&mut self, name: &str) -> Result<bool, Error>;

    fn unstash_changes(&mut self, name: &str) -> Result<bool, Error>;

    fn uncommitted_changes(&self) -> Result<Vec<String>, Error>;
}
