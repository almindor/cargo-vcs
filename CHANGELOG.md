# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

### Added
- add crate version info to verbose status

## [v0.2.0] - 2022-08-04

### Changed
- make `status` subcommand more ergonomic
- refactor `branch` subcommand into `checkout`

### Added
- add `dir` command for easier navigation to workspace members

## [v0.1.0] - 2022-07-17

### Added
- Initial release of `v0.1.0`